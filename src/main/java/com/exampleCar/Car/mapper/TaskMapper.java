package com.exampleCar.Car.mapper;

import com.exampleCar.Car.Dto.TaskDto;
import com.exampleCar.Car.entity.Task;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring" , unmappedTargetPolicy = IGNORE)
public interface TaskMapper {

    TaskDto entityToDto(Task task);
    Task dtoToEntity(TaskDto taskDto);
}

