package com.exampleCar.Car.mapper;


import com.exampleCar.Car.Dto.SubtaskDto;
import com.exampleCar.Car.entity.Subtask;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring" , unmappedTargetPolicy = IGNORE)
public interface SubtaskMapper {
    SubtaskDto entityToDto(Subtask subtask);
    Subtask dtoToEntity(SubtaskDto subtaskDto);
}


