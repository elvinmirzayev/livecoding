package com.exampleCar.Car.repository;

import com.exampleCar.Car.entity.Subtask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubtaskRepository extends JpaRepository<Subtask,Integer> {

}
