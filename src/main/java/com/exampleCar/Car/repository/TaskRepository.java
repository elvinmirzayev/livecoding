package com.exampleCar.Car.repository;

import com.exampleCar.Car.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task,Integer> {
}


