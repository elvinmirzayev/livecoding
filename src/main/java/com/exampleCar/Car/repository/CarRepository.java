package com.exampleCar.Car.repository;

import com.exampleCar.Car.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
}

