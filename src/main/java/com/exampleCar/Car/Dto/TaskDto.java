package com.exampleCar.Car.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class TaskDto {
    Integer id;
    String name;
    Boolean status;
    LocalDate deadline;
    String description;

}


