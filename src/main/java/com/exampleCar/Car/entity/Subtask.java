package com.exampleCar.Car.entity;
import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity

public class Subtask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    Boolean status;
    LocalDate deadline;
    String description;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    @ToString.Exclude

    Task task;
}

