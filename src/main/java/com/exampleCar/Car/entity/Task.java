package com.exampleCar.Car.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity

public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    Boolean status;
    LocalDate deadline;
    String description;

    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    List<Subtask>subtask;




}



