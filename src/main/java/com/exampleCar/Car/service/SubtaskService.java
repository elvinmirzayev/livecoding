package com.exampleCar.Car.service;

import com.exampleCar.Car.Dto.SubtaskDto;

public interface SubtaskService {
    SubtaskDto get(Integer subId);

    SubtaskDto create(SubtaskDto subtaskDto);

    SubtaskDto update(Integer subId, SubtaskDto subtaskDto);


}
