package com.exampleCar.Car.service;

import com.exampleCar.Car.Dto.SubtaskDto;
import com.exampleCar.Car.entity.Subtask;
import com.exampleCar.Car.mapper.SubtaskMapper;
import com.exampleCar.Car.repository.SubtaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Primary
@RequiredArgsConstructor


public class SubtaskServiceImpl implements SubtaskService{
    private final SubtaskMapper subtaskMapper;
    private final SubtaskRepository subtaskRepository;

    @Override
    public SubtaskDto get(Integer subId) {
        Subtask subtask =  subtaskRepository.findById(subId).orElseThrow(()->
                new RuntimeException("Subtask not found"));
        return subtaskMapper.entityToDto(subtask);
    }

    @Override
    public SubtaskDto create(SubtaskDto subtaskDto) {
        Subtask subtask = subtaskMapper.dtoToEntity(subtaskDto);
        subtaskRepository.save(subtask);
        return subtaskMapper.entityToDto(subtask);
    }

    @Override
    public SubtaskDto update(Integer subId, SubtaskDto subtaskDto) {
        Subtask entity = subtaskRepository.findById(subId).orElseThrow(()->
                new RuntimeException("Subtask not found"));
        entity.setStatus(subtaskDto.getStatus());
        entity.setName(subtaskDto.getName());
        entity.setDescription(subtaskDto.getDescription());
        entity.setId(subtaskDto.getId());
        entity.setDeadline(subtaskDto.getDeadline());
        entity = subtaskRepository.save(entity);
        return subtaskMapper.entityToDto(entity);

    }
}

