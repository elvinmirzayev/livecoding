package com.exampleCar.Car.service;

import com.exampleCar.Car.Dto.TaskDto;

public interface TaskService {
    TaskDto get(Integer id);

    TaskDto create(TaskDto taskDto);

    TaskDto update(Integer id, TaskDto taskDto);


}
