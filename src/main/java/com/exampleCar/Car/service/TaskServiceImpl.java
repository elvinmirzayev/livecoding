package com.exampleCar.Car.service;

import com.exampleCar.Car.Dto.TaskDto;
import com.exampleCar.Car.entity.Task;
import com.exampleCar.Car.mapper.TaskMapper;
import com.exampleCar.Car.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Primary
@RequiredArgsConstructor

public class TaskServiceImpl implements TaskService{
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;



    @Override
    public TaskDto get(Integer id) {
        Task task =  taskRepository.findById(id).orElseThrow(()->
                new RuntimeException("Task not found"));
        return taskMapper.entityToDto(task);
    }

    @Override
    public TaskDto create(TaskDto taskDto) {
        Task task = taskMapper.dtoToEntity(taskDto);
        taskRepository.save(task);
        return taskMapper.entityToDto(task);
    }

    @Override
    public TaskDto update(Integer id, TaskDto taskDto) {

        Task entity = taskRepository.findById(id).orElseThrow(()->
                new RuntimeException("Task not found"));
        entity.setStatus(taskDto.getStatus());
        entity.setName(taskDto.getName());
        entity.setDescription(taskDto.getDescription());
        entity.setId(taskDto.getId());
        entity.setDeadline(taskDto.getDeadline());
        entity = taskRepository.save(entity);
        return taskMapper.entityToDto(entity);
    }




}

